<?php

namespace App\Http\Controllers;

use App\Transaction;
use Auth;
use App\User;
use App\Category;
use App\Asset;
use App\Status;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $categories = Category::all();
        $assets = Asset::all();
        $transactions = Transaction::all();
        return view('transactions.index')->with('transactions', $transactions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $transactions = Transaction::all();
        return view('assets.index')->with('assets', $assets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $borrow = Carbon::createFromFormat('Y-m-d', $request->input('borrowDate'))->tz('UTC')->toDateString();
        $return = Carbon::createFromFormat('Y-m-d', $request->input('returnDate'))->tz('UTC')->toDateString();

        $transaction = new Transaction();
        $transaction->user_id = Auth::user()->id;
        $transaction->asset_id = $request->input('asset_id');
        $transaction->category_id = $request->input('category_id');
        $transaction->borrowDate = $request->input('borrowDate');
        $transaction->returnDate = $request->input('returnDate');
        $transaction->refNo = $transaction->user_id . "-" . $transaction->asset->serialNo . "-" . $borrow . "-" . $return;

        //dd($borrow);

        $transaction->save();
        return redirect("/assets");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        $transactions = Transaction::all();
        return view('transactions.index')->with('transactions', $transactions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$available = $request->input('available');
        
        $status = $request->input('status');

        $transaction = Transaction::find($id);
        $asset = Asset::find($transaction->asset_id);
        
        if($transaction->status_id === 1 && $asset->isAvailable === 1){
            $transaction->status_id = $status;
            $asset->isAvailable = 0;
            $asset->save();
        }

        elseif($transaction->status_id === 2 && $asset->isAvailable === 0){
            $transaction->status_id = $status;
            $asset->isAvailable = 1;
            //dd($transaction->status);
            //dd($asset->isAvailable);
            $asset->save();
        }else{
            $transaction->status_id = $status;
        }

        $transaction->save();

        return redirect("/transactions");


        /*$asset = Asset::find($request->input('id'));
        $asset->name = $request->input('name');
        $asset->description = $request->input('description');
        $asset->serialNo = $request->input('serialNo');
        $asset->category_id = $request->input('category');
        $asset->isActive = true;*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}

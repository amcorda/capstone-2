<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use App\User;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
        return view('assets.index')->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->role_id === 1){
            $categories = Category::all();
            return view('assets.create')->with('categories', $categories);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->role_id === 1){
            $rules = array(
                "name" => "required",
                "description" => "required",
                "serialNo" => "required",
                "image" => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
                "category" => "required"
            );

            $this->validate($request, $rules);

            $asset = new Asset();
            $asset->name = $request->input('name');
            $asset->description = $request->input('description');
            $asset->serialNo = $request->input('serialNo');
            $asset->category_id = $request->input('category');

            // handle image file upload
            $image = $request->file('image');

            // set the file name 
            $file_name = time() . "." . $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $file_name);

            $asset->img_path = $destination.$file_name;

            if ($asset->save()) {
                $request->session()->flash('status', 'Asset successfully added!');
                return redirect("/assets/create");
            } else {
                $request->session()->flash('status', 'Asset not added.');
                return redirect("/assets/create");
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show(Asset $asset)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->role_id === 1){
            $asset = Asset::find($id);
            $categories = Category::all();
            return view('assets.edit')->with('asset', $asset)->with('categories', $categories);  
        }   
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asset $asset)
    {

        if(Auth::user()->role_id === 1){
            $rules = array(
                "name" => "required",
                "description" => "required",
                "serialNo" => "required",
                "image" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
                "category" => "required"
            );

            $this->validate($request, $rules);
            
            $asset = Asset::find($request->input('id'));
            $asset->name = $request->input('name');
            $asset->description = $request->input('description');
            $asset->serialNo = $request->input('serialNo');
            $asset->category_id = $request->input('category');
            $asset->isActive = true;

            if($request->file('image') != null) {

                //handle image file upload
                $image = $request->file('image');
                //set the file name
                $file_name = time(). "." . $image->getClientOriginalExtension();
                $destination = "images/";
               

                unlink(public_path(). '/' .$asset->img_path);

                $asset->img_path = $destination.$file_name;
                $image->move($destination, $file_name);
            }
           
            if($asset->save()){
                $request->session()->flash('status', 'Product successfully updated');
                return redirect("/assets");
            }else{
                $request->session()->flash('status', 'Product not updated');
                return redirect("/assets");
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->role_id === 1){
                $asset = Asset::find($id);
            //toggle isActive from true to false and back
            if($asset->isActive === 1){
                $asset->isActive = 0;//setting isActive to false
                session()->flash('status', 'Product deactivated');
            } else {
                //reactivate if currently deactivated
                $asset->isActive = 1;
                session()->flash('status', 'Product reactivated');
            }
            $asset->save();
            return redirect("/assets");
        }
        
    }
}

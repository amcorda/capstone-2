<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function status()
    {
    	return $this->belongsTo('App\Status');
    }

    public function asset()
    {
    	return $this->belongsTo('App\Asset');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}

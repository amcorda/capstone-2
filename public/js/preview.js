function previewFile() {
  let preview = document.querySelector('#imgFile');
  let file    = document.querySelector('input[type=file]').files[0];
  let reader  = new FileReader();

  reader.addEventListener("load", function () {
    preview.src = reader.result;
  }, false);

  if (file) {
  	document.querySelector('#imgPreview').removeAttribute('hidden');
    reader.readAsDataURL(file);
  }
}
<?php

use Illuminate\Database\Seeder;

class TransactionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('transactions')->delete();
        
        \DB::table('transactions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'refNo' => NULL,
                'user_id' => 2,
                'status_id' => 4,
                'category_id' => 5,
                'asset_id' => 1,
                'borrowDate' => '2019-10-24 00:00:00',
                'returnDate' => '2019-10-25 00:00:00',
                'created_at' => '2019-10-22 03:36:51',
                'updated_at' => '2019-10-24 10:35:29',
            ),
            1 => 
            array (
                'id' => 2,
                'refNo' => '2-mrktbllrm-2019-10-24-2019-10-25',
                'user_id' => 2,
                'status_id' => 4,
                'category_id' => NULL,
                'asset_id' => 1,
                'borrowDate' => '2019-10-24 00:00:00',
                'returnDate' => '2019-10-25 00:00:00',
                'created_at' => '2019-10-24 09:33:08',
                'updated_at' => '2019-10-24 10:40:39',
            ),
            2 => 
            array (
                'id' => 3,
                'refNo' => '2-dlsyfnctrm-2019-10-24-2019-10-25',
                'user_id' => 2,
                'status_id' => 3,
                'category_id' => NULL,
                'asset_id' => 3,
                'borrowDate' => '2019-10-24 00:00:00',
                'returnDate' => '2019-10-25 00:00:00',
                'created_at' => '2019-10-24 10:37:57',
                'updated_at' => '2019-10-24 10:41:14',
            ),
            3 => 
            array (
                'id' => 4,
                'refNo' => '2-minithtr-2019-10-24-2019-10-25',
                'user_id' => 2,
                'status_id' => 4,
                'category_id' => NULL,
                'asset_id' => 2,
                'borrowDate' => '2019-10-24 00:00:00',
                'returnDate' => '2019-10-25 00:00:00',
                'created_at' => '2019-10-24 10:43:07',
                'updated_at' => '2019-10-24 11:09:02',
            ),
            4 => 
            array (
                'id' => 5,
                'refNo' => '2-mrktbllrm-2019-10-25-2019-10-26',
                'user_id' => 2,
                'status_id' => 5,
                'category_id' => NULL,
                'asset_id' => 1,
                'borrowDate' => '2019-10-25 00:00:00',
                'returnDate' => '2019-10-26 00:00:00',
                'created_at' => '2019-10-24 10:54:21',
                'updated_at' => '2019-10-24 10:54:58',
            ),
            5 => 
            array (
                'id' => 6,
                'refNo' => '3-minithtr-2019-11-01-2019-11-09',
                'user_id' => 3,
                'status_id' => 3,
                'category_id' => NULL,
                'asset_id' => 2,
                'borrowDate' => '2019-11-01 00:00:00',
                'returnDate' => '2019-11-09 00:00:00',
                'created_at' => '2019-10-24 11:05:18',
                'updated_at' => '2019-10-24 11:09:34',
            ),
            6 => 
            array (
                'id' => 7,
                'refNo' => '2-minithtr-2019-10-25-2019-10-26',
                'user_id' => 2,
                'status_id' => 2,
                'category_id' => NULL,
                'asset_id' => 2,
                'borrowDate' => '2019-10-25 00:00:00',
                'returnDate' => '2019-10-26 00:00:00',
                'created_at' => '2019-10-24 11:06:58',
                'updated_at' => '2019-10-24 11:08:44',
            ),
            7 => 
            array (
                'id' => 8,
                'refNo' => '2-dlsyfnctrm-2019-10-30-2019-10-31',
                'user_id' => 2,
                'status_id' => 5,
                'category_id' => NULL,
                'asset_id' => 3,
                'borrowDate' => '2019-10-30 00:00:00',
                'returnDate' => '2019-10-31 00:00:00',
                'created_at' => '2019-10-24 11:07:37',
                'updated_at' => '2019-10-24 11:07:51',
            ),
            8 => 
            array (
                'id' => 9,
                'refNo' => '2-mrktbllrm-2019-10-26-2019-10-27',
                'user_id' => 2,
                'status_id' => 3,
                'category_id' => NULL,
                'asset_id' => 1,
                'borrowDate' => '2019-10-26 00:00:00',
                'returnDate' => '2019-10-27 00:00:00',
                'created_at' => '2019-10-24 11:11:07',
                'updated_at' => '2019-10-24 11:12:21',
            ),
        ));
        
        
    }
}
<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$uSlh800Qz9aOjypZrQS2aOedx/ZGGzfAjowHwGO2YWOLa/kFNQL.q',
                'remember_token' => NULL,
                'created_at' => '2019-10-16 09:57:04',
                'updated_at' => '2019-10-16 09:57:04',
                'role_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Cardo',
                'email' => 'cardo.dalisay@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$XjQqDzkrvy6rzlD92XvbO.Jc1tK2aBK/BOWeYcoTM.hfeItteGwhu',
                'remember_token' => NULL,
                'created_at' => '2019-10-16 09:57:23',
                'updated_at' => '2019-10-16 09:57:23',
                'role_id' => 2,
            ),
        ));
        
        
    }
}
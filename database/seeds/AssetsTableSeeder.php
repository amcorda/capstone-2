<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Marikit Ballroom',
                'serialNo' => 'mrktbllrm',
                'description' => '525sqm',
                'img_path' => 'images/1571403000.jpg',
                'isActive' => 1,
                'created_at' => '2019-10-18 12:50:00',
                'updated_at' => '2019-10-18 12:50:00',
                'category_id' => 5,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Mini Theater',
                'serialNo' => 'minithtr',
                'description' => '20 seater mini theater',
                'img_path' => 'images/1571403032.jpg',
                'isActive' => 1,
                'created_at' => '2019-10-18 12:50:32',
                'updated_at' => '2019-10-18 12:50:32',
                'category_id' => 3,
            ),
        ));
        
        
    }
}
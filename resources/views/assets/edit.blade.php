@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-lg-8 offset-lg-2">

            <h3>Edit Space</h3>

            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                  {{ session()->get('status') }}
                </div>

            @endif

            <div class="card mb-3">
                
                <div class="card-header" data-toggle="collapse" href="#div-add-category">Add New Category</div>

                <div class="card-body collapse" id="div-add-category">
                    
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" id="txt-category" class="form-control">
                    </div>

                    <button type="button" class="btn btn-success" id="btn-add-category">Add category</button>

                </div>

            </div>

            <form method="post" action="/assets/{{$asset->id}}" enctype="multipart/form-data">

                @method('PUT')

                @csrf

                <input type="text" name="id" value="{{$asset->id}}" hidden>

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{$asset->name}}">
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea  name="description" id="description" class="form-control" value="{{$asset->description}}">{{$asset->description}}</textarea>
                </div>

                <div class="form-group">
                    <label for="price">Reference ID:</label>
                    <input type="text" name="serialNo" id="serialNo" class="form-control" value="{{$asset->serialNo}}">
                </div>

                <div class="form-group">
                    <label for="image">Upload image:</label>             
                        <input type="file" name="image" id="image" class="form-control" onchange="previewFile()" required>
                        <div class="row">
                            <div class="col-md-4" id="imgPreview" hidden>
                                <img class="pt-3" id="imgFile" src=""  alt="Image preview...">
                            </div>
                            <p id="imgError"></p>    
                        </div>
                </div>

                <div class="form-group">
                    <label for="category">Category:</label>
                    <select name="category" id="txt-categories" class="form-control">
                        @if (count($categories) > 0)
                            @foreach ($categories as $category)
                                @if($category->id == $asset->category_id)
                                    <option selected value="{{ $category->id }}">{{ $category->name }}</option>
                                @else
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>  

                <button type="submit" class="btn btn-success">Update Asset</button>
                <a class="btn btn-warning" href="/products">Cancel</a>

            </form>

        </div>

    </div>

    {{-- <script>
        
        const image = document.querySelector("#image");
        const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        btnAddCategory.addEventListener('click', () => {

            let formData = new FormData;
            formData.append('image', image.files[0]);

            //console.log(formData.get('image'));
            const route = 'http://localhost:8000/categories';
            const payload = {
                method: 'post',
                body: formData,
                headers: { 
                'X-CSRF-TOKEN' : CSRFToken,
                //converts responses into JSON, useful in debugging error responses
                //'Accept' : 'application/json'
                }
            };
            fetch(route, payload)
                .then((res) => {
                    //console.log(res.status);
                    if(res.status === 201){
                        addCatNotif.setAttribute('class', 'alert alert-success');
                        return res.json();
                    }else{
                        addCatNotif.setAttribute('class', 'alert alert-danger');
                        return res.json();
                    }
                })
                .then((data) => {
                    //console.log(data.data);
                    if(data.data){
                        txtCategories.innerHTML += data.data;    
                    }        
                    addCatNotif.innerHTML = data.message;
                })
        }

        /*const txtCategory = document.querySelector("#txt-category");
        const txtCategories = document.querySelector("#txt-categories");
        const btnAddCategory = document.querySelector("#btn-add-category");
        const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        btnAddCategory.addEventListener('click', () => {
            let formData = new FormData
            formData.append('category', txtCategory.value)

            const route = 'http://127.0.0.1:8000/categories'
            const payload = {
                method: 'post',
                body: formData,
                headers: { 
                    'X-CSRF-TOKEN' : CSRFToken 
                }
            }

            fetch(route, payload)
            .then((res) => {
                return res.text()
            })
            .then((data) => {
                txtCategories.innerHTML += data
            })
        })*/

    </script> --}}

    <script src="{{ asset('js/preview.js') }}"></script>
    <script src="{{ asset('js/addCat.js') }}"></script>
    <script src="{{ asset('js/addAss.js')}}"></script>

@endsection
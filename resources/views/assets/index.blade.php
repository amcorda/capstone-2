@extends('layouts.app')

@section('content')

	<h3>
		Spaces Listing
		@if(Auth::user() !== null)
			@if(Auth::user()->role_id === 1)
				<a href="/assets/create" class="btn btn-sm btn-success addAssBtn">+</a>
			@endif
		@endif
	</h3>

	@if (session()->has('status'))

        <div class="alert alert-success" role="alert">
            {{ session()->get('status') }}
        </div>

    @endif

	<hr>

	<div class="row">
		@if(Auth::user() !== null)
			{{-- if logged in user is admin --}}
			@if(Auth::user()->role_id === 1)
				{{-- product admin dashboard --}}

				<div class="row mx-3">
					@foreach($assets as $asset)

						<div class="col-6 col-md-4">
							
							<div class="card h-100 assetCard">
								
								<img src="{{asset($asset->img_path)}}" class="card-img-top border" style="height: 150px; object-fit: cover;">

								<div class="card-body h-100">
									
									<h5>{{$asset->name}}</h5>
									<p>{{$asset->description}}</p>
									<p>{{$asset->category->name}}</p>
									<small>
										Status:
										@if($asset->isActive === 1)
											Active
										@else
											Inactive
										@endif
									</small>

									<form method="post" action="/assets/{{$asset->id}}">
										@csrf
										@method('DELETE')

										<div class="btn-group btn-block">
											<a class="btn btn-primary mr-1" href="/assets/{{$asset->id}}/edit">Edit</a>
											{{-- toggle button appearance depending on current status of product's isActive property --}}
											@if($asset->isActive === 1)
												<button type="submit" class="btn btn-danger">Deactivate</button>
											@else
												<button type="submit" class="btn btn-warning">Reactivate</button>
											@endif
										</div>
									</form>
								</div>
							</div>
						</div>
					@endforeach
				</div>

			@else
				{{-- catalogue for non-admin --}}
				<div class="row mx-3">
					@foreach($assets as $asset)
						{{-- only display currently active products in the catalog --}}
						@if($asset->isActive === 1)
							<div class="col-3">
								<div class="card h-100 assetCard">
									
									<img src="{{asset($asset->img_path)}}" class="card-img-top border" style="height: 150px; object-fit: cover;">

									<div class="card-body h-100">
										<h5>{{$asset->name}}</h5>
										<p>{{$asset->description}}</p>
										<p>{{$asset->category->name}}</p>
									</div>

									<div class="card-footer h-100 text-center assetFooter">
										<!-- Button trigger modal -->
										@if($asset->isAvailable === 0)
											<small>This is not available as of now.</small>
										@else				
											<button type="button" class="btn btn-primary requestSpace" data-id="{{$asset->id}}" data-name="{{$asset->name}}" data-toggle="modal" data-target="#exampleModal">
												  Request Space
											</button>
										@endif
									</div>
								</div>
							</div>
						@endif
					@endforeach
				</div>

				<!-- Modal -->
				<form action="/transactions" method="POST">
					@csrf
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="form-group">
							<input type="hidden" id="assetInput" name="asset_id" value="">
							{{-- <input type="hidden" name="category_id" data-id="category_id" value="{{$asset->category_id}}">
						</div> --}}
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="assetTitle">Select date for</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="row">
										<div class="col-5 offset-1">
											<label>Start Date</label>
											<input class="form-control" type="date" id="borrowDate" name="borrowDate" value="borrowDate" min="2019-10-24" max="2022-12-30" required>
										</div>
												      		
										<div class="col-5 ">
											<label>Return Date</label>
											<input class="form-control" type="date" id="returnDate" name="returnDate" value="returnDate" min="2019-10-25" max="2022-12-31" required>
										</div>	   		
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Request Space</button>
								</div>
							</div>
						</div>
					</div>
				</form>


			@endif
		@endif
	</div>

	<script src="{{ asset('js/dateTimePicker.js') }}"></script>

@endsection
@extends('layouts.app')

@section('content')

    <div class="container-fluid" id="home-row">
        @if(Auth::user() !== null)
            @if(Auth::user()->role_id === 1)
                <div class="jumbotron my-4 text-center">
                    <h1 class="pb-3 header">Welcome {{ Auth::user()->name }}</h1>
                    <a class="btn btn-primary btn-lg" href="/assets" role="button">Edit Spaces</a>
                </div>
            @else
                <div class="jumbotron my-4 text-center">
                    <h1 class="pb-3 header">Welcome {{ Auth::user()->name }}</h1>
                    <a class="btn btn-primary btn-lg" href="/assets" role="button">Request Space</a>
                </div> 
            @endif

        @else
            <div class="row justify-content-center">
               {{--<img class="img-fluid" src="{{ asset('images/lobby.jpg') }}">--}}
               <div class="jumbotron jumbotron-home my-4 text-center">
                  <img src="{{ asset('images/logo.jpeg') }}" style="height: 150px">
                  <hr class="my-4">
                  <p class="header">Your gateway to first-class amenities.</p>
                  
                  <a class="btn btn-primary btn-lg" href="/login" role="button">Login</a>
                </div>
            </div>
        @endif
    </div>
@endsection

@extends('layouts.app')

@section('content')

	<h3>Requests</h3>
	<hr>

	<div class="row">
		@if(Auth::user() !== null)
			{{-- if logged in user is admin --}}
			@if(Auth::user()->role_id === 1)
				{{-- product admin dashboard --}}

				{{-- Pending Transactions --}}
				<table class="table table-bordered my-3 mx-3">
					<thead>
						<h4 class="text-center mx-3">Pending Requests</h4>
					    <tr>
					      <th scope="col">User</th>
					      <th scope="col">Space</th>

					      <th scope="col">Start Date</th>
					      <th scope="col">Return Date</th>
					      <th scope="col">Action</th>
					    </tr>
					</thead>
					
						<tbody>
						
							@foreach($transactions as $transaction)
							@if($transaction->status_id === 1)
							 	<tr>
							      <td scope="col">{{$transaction->user->name}}</td>
							      <td scope="col">{{$transaction->asset->name}}</td>
							      <td scope="col">{{$transaction->borrowDate}}</td>
							      <td scope="col">{{$transaction->returnDate}}</td>
							      <td scope="col">
							      	<div class="row">
							      		<form action="/transactions/{{$transaction->id}}" method="POST">
							      			@csrf
							      			@method('PUT')
							      			<div class="col-4">
							      				<input type="hidden" name="status" value="2">
							      				<input type="hidden" name="available" value="0">
									      		<button id="approveBtn" type="submit" class="btn btn-small btn-success" >Approve</button>
									      	</div>
									    </form>

									    <form action="/transactions/{{$transaction->id}}" method="POST">
							      			@csrf
							      			@method('PUT')
									      	<div class="col-4">
									      		<input type="hidden" name="status" value="3">
									      		<button id="rejectBtn" type="submit" class="btn btn-small btn-danger">Reject</button>
									      	</div>
									    </form>							      		
							      	</div>

							      </td>
							    </tr>
							@endif
							@endforeach
						
						</tbody>
					
				</table>

				{{-- Ongoing Transactions --}}
				<table class="table table-bordered my-3 mx-3">
					<thead>
						<h4 class="mx-3">Ongoing Requests</h4>
					    <tr>
					      <th scope="col">User</th>
					      <th scope="col">Space</th>
					      <th scope="col">Date Approved</th>
					      <th scope="col">Start Date</th>
					      <th scope="col">Return Date</th>
					      <th scope="col">Action</th>
					    </tr>
					</thead>
					
						<tbody>
						
							@foreach($transactions as $transaction)
							@if($transaction->status_id === 2)
							 	<tr>
							      <td scope="col">{{$transaction->user->name}}</td>
							      <td scope="col">{{$transaction->asset->name}}</td>
							      <td scope="col">{{$transaction->updated_at}}</td>
							      <td scope="col">{{$transaction->borrowDate}}</td>
							      <td scope="col">{{$transaction->returnDate}}</td>
							      <td scope="col">
							      	<div class="row">
							      		<form action="/transactions/{{$transaction->id}}" method="POST">
							      			@csrf
							      			@method('PUT')
							      			<div class="col-4">
							      				<input type="hidden" name="status" value="4">
							      				<input type="hidden" name="available" value="1">
									      		<button id="approveBtn" type="submit" class="btn btn-small btn-success" >Clear</button>
									      	</div>
									    </form>					      		
							      	</div>

							      </td>
							    </tr>
							@endif
							@endforeach
						</tbody>
				</table>

				{{-- Completed Transactions --}}
				<table class="table table-bordered my-3 mx-3">
					<thead>
						<h4 class="mx-3">Requests History</h4>
					    <tr>
					      <th scope="col">User</th>
					      <th scope="col">Space</th>
					      <th scope="col">Start Date</th>
					      <th scope="col">Return Date</th>
					      <th scope="col">Date Cleared/Rejected</th>
					      <th scope="col">Status</th>
					    </tr>
					</thead>
					
						<tbody>
						
							@foreach($transactions as $transaction)
							@if($transaction->status_id === 3 || $transaction->status_id === 4 || $transaction->status_id === 5)
							 	<tr>
								    <td scope="col">{{$transaction->user->name}}</td>
								    <td scope="col">{{$transaction->asset->name}}</td>
								    <td scope="col">{{$transaction->borrowDate}}</td>
								    <td scope="col">{{$transaction->returnDate}}</td>
								    <td scope="col">{{$transaction->updated_at
								    }}</td>
							      	@if($transaction->status_id === 3)
										<td class="alert alert-danger" scope="col">{{$transaction->status->name}}</td>
									@elseif($transaction->status_id === 4)
									    <td class="alert alert-info" scope="col">{{$transaction->status->name}}</td>
									@elseif($transaction->status_id === 5)
									   	<td class="alert alert-light" scope="col">{{$transaction->status->name}}</td>
									@endif							    
								</tr>
							@endif
							@endforeach
						</tbody>
				</table>




			{{-- Non Admin View --}}	
			@else
				<table class="table table-bordered my-3 mx-3">
					<h4 class="mx-3">Pending Requests</h4>
					<thead>
						<tr>
						    <th scope="col">Space</th>
						    <th scope="col">Reference Number</th>
						    <th scope="col">Start Date</th>
						    <th scope="col">Return Date</th>
						    <th scope="col">Status</th>
						    <th scope="col">Action</th>
						</tr>
					</thead>
					
						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->user_id === Auth::user()->id) {{-- Auth::user()->role_id === 1 --}}
									@if($transaction->status_id === 1)
									<tr>
										<td scope="col">{{$transaction->asset->name}}</td>
										<td scope="col">{{$transaction->refNo}}</td>
									    <td scope="col">{{$transaction->borrowDate}}</td>
									    <td scope="col">{{$transaction->returnDate}}</td>
									  	<td class="alert alert-warning" scope="col">{{$transaction->status->name}}</td>
									    <td scope="col">
									    	<div class="row">
									      		<form action="/transactions/{{$transaction->id}}" method="POST">
									      			@csrf
									      			@method('PUT')
									      			<div class="col-4">
									      				<input type="hidden" name="status" value="5">
											      		<button id="approveBtn" type="submit" class="btn btn-small btn-danger">Cancel</button>
											      	</div>
											    </form>					      		
									      	</div>
									    </td>	
								    </tr>
								    @endif
							    @endif
							@endforeach
						</tbody>
				</table>

				{{-- Ongoing Transactions --}}
				<table class="table table-bordered my-3 mx-3">
					<h4 class="mx-3">Ongoing Requests</h4>
					<thead>
						<tr>
						    <th scope="col">Space</th>
						    <th scope="col">Reference Number</th>
						    <th scope="col">Date Approved</th>
						    <th scope="col">Start Date</th>
						    <th scope="col">Return Date</th>
						    <th scope="col">Status</th>
		
						</tr>
					</thead>
					
						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->user_id === Auth::user()->id) {{-- Auth::user()->role_id === 1 --}}
									@if($transaction->status_id === 2)
									<tr>
										<td scope="col">{{$transaction->asset->name}}</td>
										<td scope="col">{{$transaction->refNo}}</td>
										<td scope="col">{{$transaction->updated_at}}</td>
									    <td scope="col">{{$transaction->borrowDate}}</td>
									    <td scope="col">{{$transaction->returnDate}}</td>
									    <td class="alert alert-success" scope="col">{{$transaction->status->name}}</td>	   
								    </tr>
								    @endif
							    @endif
							@endforeach
						</tbody>
				</table>

				{{-- Transactions History --}}
				<table class="table table-bordered my-3 mx-3">
					<h4 class="mx-3">Transaction History</h4>
					<thead>
						<tr>
						    <th scope="col">Space</th>
						    <th scope="col">Reference Number</th>
						    <th scope="col">Start Date</th>
						    <th scope="col">Return Date</th>
						    <th scope="col">Date Cleared/Rejected</th>
						    <th scope="col">Status</th>
		
						</tr>
					</thead>
					
						<tbody>
							@foreach($transactions as $transaction)
								@if($transaction->user_id === Auth::user()->id) {{-- Auth::user()->role_id === 1 --}}
									@if($transaction->status_id === 3 || $transaction->status_id === 4 || $transaction->status_id === 5)
									<tr>
										<td scope="col">{{$transaction->asset->name}}</td>
										<td scope="col">{{$transaction->refNo}}</td>
									    <td scope="col">{{$transaction->borrowDate}}</td>
									    <td scope="col">{{$transaction->returnDate}}</td>
									    <td scope="col">{{$transaction->updated_at}}</td>
									    @if($transaction->status_id === 3)
									    	<td class="alert alert-danger" scope="col">{{$transaction->status->name}}</td>
									    @elseif($transaction->status_id === 4)
									    	<td class="alert alert-info" scope="col">{{$transaction->status->name}}</td>
									    @elseif($transaction->status_id === 5)
									    	<td class="alert alert-light" scope="col">{{$transaction->status->name}}</td>
									    @endif	
								    </tr>
								    @endif
							    @endif
							@endforeach
						</tbody>
				</table>

					
			@endif
		@endif
	</div>
	
	<script src="{{ asset('js/updateStatus.js') }}"></script>
	
@endsection